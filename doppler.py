import tkinter

WIDTH = 1024
HEIGHT = 768
FPS = 30
LIFE_OF_RING = 60
SPEED_OF_SOUND = 3

sources = []
rings = []

win = tkinter.Tk()
canvas = tkinter.Canvas(win, width=WIDTH, height=HEIGHT, bg="black")
canvas.pack()

class Source:
    def __init__(self, x0, y0, freq=15, speed=2, acc=0):
        self.x = x0
        self.y = y0
        self.speed = speed
        self.acc = acc
        self.dia = 10
        self.freq = freq
        self.state = 0
        sources.append(self)
    
    def draw(self):
        canvas.create_oval(self.x-self.dia/2, self.y-self.dia/2, self.x+self.dia/2, self.y+self.dia/2, fill="red")

    def tick(self):
        self.x += self.speed
        self.speed += self.acc
        self.state += 1
        if self.state == self.freq:
            self.state = 0
            Ring(self.x, self.y)
        self.draw()

class Ring:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.life = LIFE_OF_RING
        self.dia = 10
        rings.append(self)

    def draw(self):
        canvas.create_oval(self.x-self.dia/2, self.y-self.dia/2, self.x+self.dia/2, self.y+self.dia/2, outline="white")

    def tick(self):
        if self.life <= 0:
            del self
            return
        self.dia += SPEED_OF_SOUND * 2
        self.life -= 1
        self.draw()


# Ide jönnek a források
Source(50, HEIGHT/3, speed=2)

def tick():
    canvas.delete("all")
    for source in sources:
        source.tick()

    for ring in rings:
        ring.tick()

    win.after(int(1/FPS*1000), tick)

tick()

win.mainloop()